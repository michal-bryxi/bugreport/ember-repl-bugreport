import Component from '@glimmer/component';
import { compileHBS } from 'ember-repl';
import BarTemplate from './bar';
export default class FooComponent extends Component {
  myComponent = compileHBS('<Bar />', { scope: { Bar: BarTemplate } });
}
